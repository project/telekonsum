api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
; Download the install profile and recursively build all its dependencies:
projects[telekonsum][download][type] = git
projects[telekonsum][download][url] = http://git.drupal.org/project/telekonsum.git
projects[telekonsum][download][branch] = 7.x-1.x
projects[telekonsum][type] = profile