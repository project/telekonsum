<?php
/**
 * @file
 * telekonsum_user_profiles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function telekonsum_user_profiles_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
