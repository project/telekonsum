<?php
/**
 * @file
 * telekonsum_product.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function telekonsum_product_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'product_search';
  $ds_view_mode->label = 'Product Search';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'commerce_product' => 'commerce_product',
  );
  $export['product_search'] = $ds_view_mode;

  return $export;
}
