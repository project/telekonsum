<?php
/**
 * @file
 * telekonsum_add_to_cart_block.features.inc
 */

/**
 * Implements hook_views_api().
 */
function telekonsum_add_to_cart_block_views_api() {
  return array("api" => "3.0");
}
