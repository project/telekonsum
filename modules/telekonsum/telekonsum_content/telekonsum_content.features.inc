<?php
/**
 * @file
 * telekonsum_content.features.inc
 */

/**
 * Implements hook_node_info().
 */
function telekonsum_content_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Create a basic content page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
