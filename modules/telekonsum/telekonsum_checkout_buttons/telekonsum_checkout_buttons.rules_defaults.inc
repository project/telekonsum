<?php
/**
 * @file
 * telekonsum_checkout_buttons.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function telekonsum_checkout_buttons_default_rules_configuration() {
  $items = array();
  $items['rules_change_checkout_button_value'] = entity_import('rules_config', '{ "rules_change_checkout_button_value" : {
      "LABEL" : "Change checkout button value",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Telekonsum" ],
      "REQUIRES" : [ "commerce_rules_extra" ],
      "ON" : { "process_checkout_page" : [] },
      "DO" : [
        { "commerce_rules_extra_change_page" : { "page_id" : "review", "submit_value" : "Order now" } }
      ]
    }
  }');
  return $items;
}
