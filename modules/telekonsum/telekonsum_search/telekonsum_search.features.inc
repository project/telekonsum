<?php
/**
 * @file
 * telekonsum_search.features.inc
 */

/**
 * Implements hook_views_api().
 */
function telekonsum_search_views_api() {
  return array("api" => "3.0");
}
