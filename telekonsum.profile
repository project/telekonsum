<?php

/**
 * Implements hook_form_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function telekonsum_form_install_configure_form_alter(&$form, $form_state) {
  // Set a default name for the dev site and change title's label.
  $form['site_information']['site_name']['#title'] = 'Store name';
  $form['site_information']['site_mail']['#title'] = 'Store email address';
  $form['site_information']['site_name']['#default_value'] = t('Telekonsum');

  // Set a default country so we can benefit from it on Address Fields.
  $form['server_settings']['site_default_country']['#default_value'] = 'DE';

  // Hide Update Notifications.
  $form['update_notifications']['#access'] = FALSE;
}

/**
 * Implements hook_install_tasks_alter().
 */
function telekonsum_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_select_profile']['display'] = FALSE;
  $welcome['telekonsum_welcome_message'] = array(
    'display_name' => st('Welcome'),
    'display' => TRUE,
    'type' => 'form',
    'run' => isset($install_state['parameters']['welcome']) ? INSTALL_TASK_SKIP : INSTALL_TASK_RUN_IF_REACHED,
  );
  $old_tasks = $tasks;
  $tasks = array_slice($old_tasks, 0, 1) + $welcome+ array_slice($old_tasks, 1);
  _telekonsum_set_theme('telekonsum_admin');
}


function telekonsum_welcome_message($form, $form_state){
  drupal_set_title(st('Welcome'));
  $welcome = st('<p>The following steps will guide you throuh the installation '
  . 'and configuration of your new Telekonsum Online-Shop. Feel free to contact us for '
  . 'service requests.</p>');
  $form = array();
  $form['welcome_message'] = array(
    '#markup' => $welcome);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Install Telekonsum'),
  );
  return $form;
}

function telekonsum_welcome_message_submit($form, &$form_state) {
  global $install_state;
  $install_state['parameters']['welcome'] = 'done';
}

/**
 * Implements hook_install_tasks().
 */
function telekonsum_install_tasks() {
  // Remove any status messages that might have been set. They are unneeded.
  drupal_get_messages('status', TRUE);

  $tasks = array();
  $current_task = variable_get('install_task', 'done');
  $install_demo_store = variable_get('commerce_kickstart_demo_store', FALSE);

  $tasks['telekonsum_configure_store_form'] = array(
    'display_name' => st('Configure Telekonsum'),
    'type' => 'form',
  );
  $tasks['telekonsum_configure_tax_form'] = array(
    'display_name' => st('Configure Taxes'),
    'type' => 'form',
  );
  $tasks['telekonsum_install_additional_modules'] = array(
    'display_name' => st('Install additional functionality'),
    'type' => 'batch',
  );
  $tasks['telekonsum_import_content'] = array(
    'display_name' => st('Import content'),
    'type' => 'batch',
  );

  return $tasks;
}

/**
 * Task callback: returns the form allowing the user to add example store content on install.
 */
function telekonsum_configure_store_form() {
  include_once DRUPAL_ROOT . '/includes/iso.inc';

  drupal_set_title(st('Configure store'));
  
  $form['owner'] = array(
    '#type' => 'fieldset',
    '#title' => st('Store owner')
  );
  $form['owner']['company'] = array(
    '#type' => 'textfield',
    '#title' => st('Company')
  );
  $form['owner']['street'] = array(
    '#type' => 'textfield',
    '#title' => st('Street')
  );
  $form['owner']['zip'] = array(
    '#type' => 'textfield',
    '#title' => st('Zip code'),
    '#size' => 5
  );
  $form['owner']['city'] = array(
    '#type' => 'textfield',
    '#title' => st('City'),
    '#size' => 20
  );
  $form['owner']['imprint'] = array(
    '#type' => 'checkbox',
    '#title' => st('Create a basic imprint page'),
    '#description' => st('Attention: you have to ensure the legal compliance your country!')
  );

  $form['country'] = array(
    '#type' => 'fieldset',
    '#title' => st('Country'),
  );
  $form['country']['country_list'] = array(
    '#title' => t('Default store country'),
    '#description' => t('Services specific to the selected country will be installed if they exist.'),
    '#options' => _country_get_predefined_list(),
    '#type' => 'select',
    '#default_value' => variable_get('site_default_country'),
    // This field is not required when installing through drush.
    '#required' => !drupal_is_cli(),
  );

  // Prepare all the options for sample content.
  $options = array(
    '1' => st('Yes'),
    '0' => st('No'),
  );
  $form['localization'] = array(
    '#type' => 'fieldset',
    '#title' => st('Localization'),
  );
  $form['localization']['install_localization'] = array(
    '#type' => 'radios',
    '#title' => st('Do you want to be able to translate the interface of your store?'),
    '#options' => $options,
    '#default_value' => '0',
  );

  $options_selection = array(
    'anonymous_checkout' => 'Allow checkout for <strong>anonymous users</strong>.',
    'coupon_system' => 'Install coupon system.',
  );
  $form['functionality']['extras'] = array(
    '#type' => 'checkboxes',
    '#options' => $options_selection,
    '#title' => t("Install additional functionality"),
    '#states' => array(
      'visible' => array(
        ':input[name="install_demo_store"]' => array('value' => '0'),
      ),
    ),
  );

  // Build a currency options list from all defined currencies.
  $options = array();
  foreach (commerce_currencies(FALSE, TRUE) as $currency_code => $currency) {
    $options[$currency_code] = t('@code - !name', array(
      '@code' => $currency['code'],
      '@symbol' => $currency['symbol'],
      '!name' => $currency['name']
    ));

    if (!empty($currency['symbol'])) {
      $options[$currency_code] .= ' - ' . check_plain($currency['symbol']);
    }
  }

  $form['commerce_default_currency_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => st('Currency'),
  );
  $form['commerce_default_currency_wrapper']['commerce_default_currency'] = array(
    '#type' => 'select',
    '#title' => t('Default store currency'),
    '#options' => $options,
    '#default_value' => 'EUR'
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Create'),
    '#weight' => 15,
  );
  return $form;
}

/**
 * Submit callback: creates the requested sample content.
 */
function telekonsum_configure_store_form_submit(&$form, &$form_state) {
  variable_set('telekonsum_store_country', $form_state['values']['country_list']);
  variable_set('telekonsum_localization', $form_state['values']['install_localization']);
  variable_set('telekonsum_selected_extras', $form_state['values']['extras']);
  variable_set('commerce_default_currency', $form_state['values']['commerce_default_currency']);
  variable_set('commerce_enabled_currencies', array($form_state['values']['commerce_default_currency'] => $form_state['values']['commerce_default_currency']));
  
  variable_set('telekonsum_store_owner_company', $form_state['values']['company']);
  variable_set('telekonsum_store_owner_street', $form_state['values']['street']);
  variable_set('telekonsum_store_owner_zip', $form_state['values']['zip']);
  variable_set('telekonsum_store_owner_city', $form_state['values']['city']);
}

/**
 * Task callback: returns the form allowing the user to add example store content on install.
 */
function telekonsum_configure_tax_form() {
  drupal_set_title(st('Configure taxes'));
  
  // Prepare all the options for sample content.
  $options = array(
    'none' => st("No sample tax rate."),
    'europe' => st('European - Inclusive tax rates (VAT)'),
    'us' => st('US - Sales taxes displayed in checkout'),
  );
  $form['commerce_kickstart_tax_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => st('Tax Rate'),
  );
  $form['commerce_kickstart_tax_wrapper']['commerce_kickstart_choose_tax_country'] = array(
    '#type' => 'radios',
    '#title' => st('Tax rate examples'),
    '#description' => st('Example tax rates will be created in this style.'),
    '#options' => $options,
    '#default_value' => 'europe',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Create and Finish'),
    '#weight' => 15,
  );
  
  return $form;
}

/**
 * Submit callback: creates the requested sample content.
 */
function telekonsum_configure_tax_form_submit(&$form, &$form_state) {
  variable_set('telekonsum_choose_tax_country', $form_state['values']['commerce_kickstart_choose_tax_country']);
}

function telekonsum_install_additional_modules() {
  $modules = array();
  
  $selected_extras = variable_get('telekonsum_selected_extras', array());
  if (empty($selected_extras['anonymous_checkout'])) {
    $modules[] = 'commerce_checkout_redirect';
  }
  if (!empty($selected_extras['coupon_system'])) {
    $modules[] = 'commerce_coupon';
    $modules[] = 'commerce_coupon_ui';
    $modules[] = 'commerce_coupon_fixed_amount';
    $modules[] = 'commerce_coupon_pct';
  }  
  
  $install_localization = variable_get('telekonsum_localization', FALSE);
  if ($install_localization) {
    $modules[] = 'locale';
    $modules[] = 'variable';
    $modules[] = 'i18n';
    $modules[] = 'i18n_field';
  }

  $store_country = variable_get('telekonsum_store_country', 'DE');
  
  $eu_country_codes = array(
    'AT',
    'BE',
    'BG',
    'CY',
    'CZ',
    'DE',
    'DK',
    'EE',
    'ES',
    'FI',
    'FR',
    'GB',
    'GR',
    'HU',
    'IE',
    'IT',
    'LT',
    'LU',
    'LV',
    'MT',
    'NL',
    'PL',
    'PT',
    'RO',
    'SE',
    'SI',
    'SK',
  );

  // Resolve the dependencies now, so that module_enable() doesn't need
  // to do it later for each individual module (which kills performance).
  $files = system_rebuild_module_data();
  $modules_sorted = array();
  foreach ($modules as $module) {
    if ($files[$module]->requires) {
      // Create a list of dependencies that haven't been installed yet.
      $dependencies = array_keys($files[$module]->requires);
      $dependencies = array_filter($dependencies, '_telekonsum_filter_dependencies');
      // Add them to the module list.
      $modules = array_merge($modules, $dependencies);
    }
  }
  $modules = array_unique($modules);
  foreach ($modules as $module) {
    $modules_sorted[$module] = $files[$module]->sort;
  }
  arsort($modules_sorted);

  $operations = array();
  
  foreach ($modules_sorted as $module => $weight) {
    $operations[] = array('_telekonsum_enable_module', array($module, $files[$module]->info['name']));
  }
  if ($install_localization) {
    $operations[] = array('_telekonsum_setup_localization', array(t('Configured localization.')));
  }
  
  $operations[] = array('_telekonsum_taxonomy_menu', array(t('Setting up menus')));
  
  $operations[] = array('_telekonsum_flush_caches', array(t('Flushed caches.')));
  
  $batch = array(
    'title' => t('Installing additional functionality'),
    'operations' => $operations,
    'file' => drupal_get_path('profile', 'telekonsum') . '/telekonsum.install_callbacks.inc',
  );

  return $batch;
}

/**
 * array_filter() callback used to filter out already installed dependencies.
 */
function _telekonsum_filter_dependencies($dependency) {
  return !module_exists($dependency);
}


/**
 * Task callback: return a batch API array with the products to be imported.
 */
function telekonsum_import_content() {

  $operations = array();
  $operations[] = array('_telekonsum_example_taxes', array(t('Setting up taxes.')));

  $batch = array(
    'title' => t('Import content'),
    'operations' => $operations,
    'file' => drupal_get_path('profile', 'telekonsum') . '/telekonsum.install_callbacks.inc',
  );

  return $batch;
}

/**
 * Forces to set the erpal_maintenance theme during the installation
 */
function _telekonsum_set_theme($target_theme) {
  if ($GLOBALS['theme'] != $target_theme) {
    unset($GLOBALS['theme']);
    drupal_static_reset();
    $GLOBALS['conf']['maintenance_theme'] = $target_theme;
    _drupal_maintenance_theme();
  }
}