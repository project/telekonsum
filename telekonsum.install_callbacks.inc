<?php

/**
 * @file
 * Contains Batch API callbacks used during installation.
 */

/**
 * BatchAPI callback.
 *
 * @see telekonsum_install_additional_modules()
 */
function _telekonsum_enable_theme($theme, &$context) {
  theme_enable(array($theme));
  variable_set('theme_default', $theme);

  $context['message'] = st('Installed the default theme.');
}

/**
 * BatchAPI callback.
 *
 * @see telekonsum_install_additional_modules()
 */
function _telekonsum_enable_module($module, $module_name, &$context) {
  module_enable(array($module), FALSE);
  $context['message'] = st('Installed %module module.', array('%module' => $module_name));
}

/**
 * BatchAPI callback.
 *
 * @see telekonsum_install_additional_modules()
 */
function _telekonsum_setup_localization($operation, &$context) {
  require_once DRUPAL_ROOT . '/includes/language.inc';
  $context['message'] = t('@operation', array('@operation' => $operation));

  // Enable en prefix for english language.
  db_update('languages')
    ->fields(array(
      'prefix' => 'en',
    ))
    ->condition('language',  'en')
    ->execute();

  // Enable language detection via url.
  $negotiation['locale-url'] = array(
    'types' => array(
      'language_content',
      'language',
      'language_url',
    ),
    'callbacks' => array(
      'language' => 'locale_language_from_url',
      'switcher' => 'locale_language_switcher_url',
      'url_rewrite' => 'locale_language_url_rewrite_url',
    ),
    'file' => 'includes/locale.inc',
    'weight' => '-8,',
    'name' => 'URL',
    'description' => t('Determine the language from the URL (Path prefix or domain).'),
    'config' => 'admin/config/regional/language/configure/url',
  );
  language_negotiation_set('language', $negotiation);
}

/**
 * BatchAPI callback.
 *
 * @see telekonsum_install_additional_modules()
 */
function _telekonsum_flush_caches($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));
  drupal_flush_all_caches();
}

/**
 * BatchAPI callback.
 *
 * @see telekonsum_import_content()
 */
function _telekonsum_example_taxes($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));

  $telekonsum_choose_tax_country = variable_get('telekonsum_choose_tax_country', NULL);

  // Create the choosen tax.
  if (!empty($telekonsum_choose_tax_country)) {
    if ($telekonsum_choose_tax_country == 'us') {
      $tax = array(
        'name' => 'sample_michigan_sales_tax',
        'title' => 'Sample Michigan Sales Tax 6%',
        'display_title' => 'Sample Michigan Sales Tax 6%',
        'description' => '',
        'rate' => 0.06,
        'type' => 'sales_tax', // vat
        'rules_component' => '',
        'default_rules_component' => FALSE,
        'tax_component' => '',
        'admin_list' => TRUE,
        'calculation_callback' => 'commerce_tax_rate_calculate',
        'module' => 'commerce_tax_ui',
        'is_new' => TRUE,
      );
      commerce_tax_ui_tax_rate_save($tax);

      $rule = '{ "commerce_tax_rate_sample_michigan_sales_tax" : {
        "LABEL" : "Calculate Sample Michigan Sales Tax 6%",
        "PLUGIN" : "rule",
        "REQUIRES" : [ "commerce_order", "commerce_tax" ],
        "USES VARIABLES" : {
          "commerce_line_item" : {
            "label" : "Line item",
            "type" : "commerce_line_item"
          }
        },
        "IF" : [{
          "commerce_order_compare_address" : {
            "commerce_order" : [ "commerce-line-item:order" ],
            "address_field" : "commerce_customer_shipping|commerce_customer_address",
            "address_component" : "administrative_area",
            "value" : "MI"
          }
        }],
        "DO" : [{
          "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "sample_michigan_sales_tax"},
              "PROVIDE" : {
                "applied_tax" : {
                  "applied_tax" : "Applied tax"
                }
              }
            }
          }
        ]}
      }';
      $rules_config = rules_import($rule);
      $rules_config->save();

      $tax = array(
        'name' => 'sample_michigan_sales_tax',
        'rules_component' => 'commerce_tax_rate_sample_michigan_sales_tax',
        'is_new' => FALSE,
      );
      commerce_tax_ui_tax_rate_save($tax);
    }

    if ($telekonsum_choose_tax_country == 'europe') {
      variable_set('import_choosen_tax', 'europe');
      $tax = array(
        'name' => 'german_vat_tax',
        'title' => 'German VAT 19%',
        'display_title' => 'German VAT 19%',
        'description' => '',
        'rate' => 0.19,
        'type' => 'vat', // vat
        'default_rules_component' => TRUE,
        'tax_component' => '',
        'admin_list' => TRUE,
        'calculation_callback' => 'commerce_tax_rate_calculate',
        'module' => 'commerce_tax_ui',
        'is_new' => TRUE,
      );
      commerce_tax_ui_tax_rate_save($tax);
    }
  }
  // delete the variables we used during this task.
  variable_del('telekonsum_choose_tax_country');
}

function _telekonsum_taxonomy_menu($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));

  // Set up taxonomy main menu.
  $vocabulary_machine_name = 'category';
  if ($collection = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name)) {
    $variable_name = _taxonomy_menu_build_variable('vocab_menu', $collection->vid);
    variable_set($variable_name, 'main-menu');
    $variable_name = _taxonomy_menu_build_variable('vocab_parent', $collection->vid);
    variable_set($variable_name, '0');
    // $variable_name = _taxonomy_menu_build_variable('path', $collection->vid);
    // variable_set($variable_name, 'telekonsum_taxonomy_term_path');
    $variable_name = _taxonomy_menu_build_variable('rebuild', $collection->vid);
    variable_set($variable_name, 1);
    $variable_name = _taxonomy_menu_build_variable('sync', $collection->vid);
    variable_set($variable_name, 1);
  }
}